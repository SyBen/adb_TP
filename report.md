ABD - Spark Lab
=====

Integral
===
First, we get the function associated to the integral :
integral = ln(10) - ln(1) with ln(1) = 0
So, integral = ln(10).

Digram
===
Currently the number of partitions is reduced programatically to 1 to remove missed digrams.
The drawback is that it does not scale.
The number of missed digrams is given by : 
```missedDigram = nbPartitions - 1```

ParallelDigram
===
As an additional constraint, we are now interested only by digrams in the same sentence.
The original text file has also been stripped of its line carriages, replaced by spaces.

Both digram jobs are case sensitive.
