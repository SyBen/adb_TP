import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

import scala.collection.mutable.ListBuffer

object mySparkApp {
    val conf = new SparkConf
    conf.setMaster("local[4]")
    val sc = new SparkContext()

    def main(args: Array[String]) {
        if(args.length < 1) {
            println("No operation chosen. Shutting down.")
        }

        //Integral case : work for both sequential and parallel runs
        else if(args(0) == "integral") {
            println("Integral : " + sc.parallelize(0 to 99).map(i => 9.0/100.0 * (1.0/(1.0 + i * (9.0/100.0)))).reduce((a, b) => a + b))
        }

        //Sequential digram case
        else if (args(0) == "digram") {
            val digrams = sc.textFile("pg17489.txt").flatMap(_.split("\\P{L}+")).filter(i => i != "").coalesce(1).mapPartitions( w => {
                        val it = w
                        val lb = new ListBuffer[Tuple2[String,String]]
                        var old = it.next
                        var current = it.next
                        lb += new Tuple2(old, current)

                        while(it.hasNext) {
                            old = current
                            current = it.next
                            lb += new Tuple2(old, current)
                        }
            lb.iterator
            }).map((_, 1)).reduceByKey(_ + _).sortBy(i => i._2)
            digrams.saveAsTextFile("file:///home/syben/Documents/abd/spark/digrams")
            println("number of digrams : " + digrams.count)
            println("number of partitions : " + digrams.partitions.length)
        }

        //Parallel digram case
        else if (args(0) == "digramP") {
            val digrams = sc.textFile("pg17489.txt").flatMap(_.split("\\.")).flatMap(w => {
                val it = w.split("\\P{L}+").filter(i => i != "").iterator;
                val lb = new ListBuffer[Tuple2[String,String]]
                if(it.hasNext) {
                    var old = it.next
                    if(it.hasNext) {
                        var current = it.next 
                        lb += new Tuple2(old, current) 
                        while(it.hasNext) {
                             old = current
                             current = it.next
                             lb += new Tuple2(old, current)
                        }
                    }
                } 
                lb
           }).map((_, 1)).reduceByKey(_ + _).sortBy(i => i._2)
        digrams.saveAsTextFile("file:///home/syben/Documents/abd/spark/digramsP")
        } else if (args(0) == "performances") {
            var total = Int.int2long(0)
            for(i <- 0 to 9) {
                var time = System.currentTimeMillis
                mySparkApp.main(args.tail)
                total += (System.currentTimeMillis - time)
            }
            println("Time spent : " + total/10.0 + "ms")
        }
    }
}
